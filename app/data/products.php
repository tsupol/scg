<?php
$data = array();

// duplicate here
ob_start(); ?>
  <p class="_emphasis">
    SCG hosts SCG TOP DEALER AWARD 2017 at the
    Convention Hall of Centara Grand at Central World,
  </p>
  <p>
    gratitude in the dedication, determination and collaboration
    of all dealers of SCG’s constructionmaterials both inside and
    outside of ASEAN throughout 2017. The event welcomes
    over 300 attendees with the theme “Flying Beyond Tomorrow”
    and a “swan” symbolishing success and the future of greater
    prosperity and strenght.
  </p>
<?php $data[] = array(
  "title"     => "Industrial Products",
  "excerpt"  => "SCG International has a supply chain that spans the globe and we are always ready to serve our customers’ industrial and recycled product needs. With the changing global economy, manufacturers’ needs are also changing and we are continuously learning and adapting our supply chain to meet those demands. SCG International’s industrial and recycled products are suitable for a variety of industrial uses to suit the needs of customers as per their requirements.",
  "content"   => ob_get_clean(),
  "thumb"     => "./imgs/products/product-1.jpg",
  "images"    => [
    "./imgs/products/product-1.jpg",
    "./imgs/products/product-2.jpg",
  ],
);
// duplicate here
ob_start(); ?>
  <p class="_emphasis">
    SCG hosts SCG TOP DEALER AWARD 2017 at the
    Convention Hall of Centara Grand at Central World,
  </p>
  <p>
    gratitude in the dedication, determination and collaboration
    of all dealers of SCG’s constructionmaterials both inside and
    outside of ASEAN throughout 2017. The event welcomes
    over 300 attendees with the theme “Flying Beyond Tomorrow”
    and a “swan” symbolishing success and the future of greater
    prosperity and strenght.
  </p>
<?php $data[] = array(
  "title"     => "Construction Materials",
  "excerpt"  => "We are a leading integrated manufacturer, supplier, and distributor of cement, construction materials and related building materials. We provide an extensive range of construction products. We have pushed for development in this sector to increase operational efficiency, and speed up the process of development of new products and services. SCG International takes a role to support SCG Cement-Building Materials to expand the products to ASEAN and the global market.",
  "content"   => ob_get_clean(),
  "thumb"     => "./imgs/products/product-2.jpg",
  "images"    => [
    "./imgs/products/product-1.jpg",
    "./imgs/products/product-2.jpg",
  ],
);
// duplicate here
ob_start(); ?>
  <p class="_emphasis">
    SCG hosts SCG TOP DEALER AWARD 2017 at the
    Convention Hall of Centara Grand at Central World,
  </p>
  <p>
    gratitude in the dedication, determination and collaboration
    of all dealers of SCG’s constructionmaterials both inside and
    outside of ASEAN throughout 2017. The event welcomes
    over 300 attendees with the theme “Flying Beyond Tomorrow”
    and a “swan” symbolishing success and the future of greater
    prosperity and strenght.
  </p>
<?php $data[] = array(
  "title"     => "Commodity Products",
  "excerpt"  => "Energy is a key component that drives the industrial sector. SCG International has the capabilities and resources to supply high-quality coal and commodity products to specific industries such as power plants, cement, paper, food, and other industries throughout Thailand and Asia. We also have an efficient supply chain that starts from sourcing all the way to delivery of the products to the customer’s factories.
",
  "content"   => ob_get_clean(),
  "thumb"     => "./imgs/products/product-3.jpg",
  "images"    => [
    "./imgs/products/product-1.jpg",
    "./imgs/products/product-2.jpg",
  ],
);

if(isset($_GET['index'])) {
  echo json_encode($data[$_GET['index']]);
} else {
  echo json_encode($data);
}
