<?php
// $name = 'Tester';
// $tel = '12345';
// $company_name = '-';
// $description = 'This is the message';
// $email = 'test@gmail.com';

$the_date = date("F Y");
$color1 = '#ffffff';

$style = array(
  'footer-link'      => "text-decoration: underline; 
                      color: {$color1}; 
                      margin: 0 10px; 
                      letter-spacing: 2px",
  'date-wrap'        => "text-align: right;
                      color: #7f7f7f;
                      text-transform: uppercase;
                      letter-spacing: 1px;
                      padding: 10px 20px;",
  'content-wrap'     => "padding: 40px 20px 60px 20px;",
  'title'            => "font-size: 16px; margin: 10px 0;",
  'p-margin'         => "margin: 40px 0 0 0; color: #000000 !important; line-height: 22px;",
  'message'          => "font-weight: bold; color: #000000 !important; line-height: 22px;",
  'footer-link-wrap' => "text-align: center; padding: 10px;",
  'copyright'        => "font-family: 'Helvetica Neue', sans-serif;
                        text-align: center;
                        color: #ffffff !important;
                        font-size: 12px;
                        font-weight: 400;
                        line-height: 18px;
                        padding: 30px 0 0 0;",
  'footer-padding'   => 'padding: 40px 10px;',
)
?>
<!DOCTYPE html>
<center style="width: 100%; background-color: #eeeeee;">
  <!-- Progressive Enhancements : BEGIN -->
  <!--[if mso | IE]>
  <table role="presentation" border="0" cellpadding="0" cellspacing="0" width="100%" style="background-color: #eeeeee;">
    <tr>
      <td>
  <![endif]-->

  <!-- Visually Hidden Preheader Text : BEGIN -->
  <div style="display: none; font-size: 1px; line-height: 1px; max-height: 0px; max-width: 0px; opacity: 0; overflow: hidden; mso-hide: all; font-family: sans-serif;">
    SCG International Contact
  </div>
  <!-- Visually Hidden Preheader Text : END -->

  <!-- Create white space after the desired preview text so email clients don’t pull other distracting text into the inbox preview. Extend as necessary. -->
  <!-- Preview Text Spacing Hack : BEGIN -->
  <div style="display: none; font-size: 1px; line-height: 1px; max-height: 0px; max-width: 0px; opacity: 0; overflow: hidden; mso-hide: all; font-family: sans-serif;">
    &zwnj;&nbsp;&zwnj;&nbsp;&zwnj;&nbsp;&zwnj;&nbsp;&zwnj;&nbsp;&zwnj;&nbsp;&zwnj;&nbsp;&zwnj;&nbsp;&zwnj;&nbsp;&zwnj;&nbsp;&zwnj;&nbsp;&zwnj;&nbsp;&zwnj;&nbsp;&zwnj;&nbsp;&zwnj;&nbsp;&zwnj;&nbsp;&zwnj;&nbsp;&zwnj;&nbsp;
  </div>
  <!-- Preview Text Spacing Hack : END -->

  <!-- Email Body : BEGIN -->
  <table align="center" role="presentation" cellspacing="0" cellpadding="0" border="0" width="600" style="margin: 0 auto; font-family: 'Helvetica Neue', sans-serif;" class="email-container">
    <!------------------------------------->
    <!------------------------------------->
    <!----- Header ----->
    <!------------------------------------->
    <!------------------------------------->
    <tr>
      <td style="background-color: #ffffff;">
        <table role="presentation" cellspacing="0" cellpadding="0" border="0" width="100%">
          <tr>
            <td style="padding: 30px 20px 20px 20px">
              <img src="<?php echo($absolute_url) ?>/imgs/logo@2x.png" width="150" height="63" alt="SCG"
            </td>
            <td style="<?php echo($style['date-wrap']) ?>">
              <?php echo($the_date) ?>
            </td>
          </tr>
        </table>
      </td>
    </tr>

    <!------------------------------------->
    <!------------------------------------->
    <!----- Content ----->
    <!------------------------------------->
    <!------------------------------------->
    <tr>
      <td style="background-color: #ffffff;">
        <table role="presentation" cellspacing="0" cellpadding="0" border="0" width="100%">
          <tr>
            <td style="<?php echo($style['content-wrap']) ?>">
              <h3 style="<?php echo($style['title']) ?>">Dear, <?php echo($name) ?>, <?php echo($company_name) ?></h3>
              <h3 style="<?php echo($style['title']) ?>"><?php echo($tel) ?></h3>
              <h3 style="<?php echo($style['title']) ?>"><?php echo($email) ?></h3>
              <p style="<?php echo($style['p-margin']) ?>">
                Thank you for interesting in our services and solutions.
                We have received your message...
              </p>
              <p style="<?php echo($style['message']) ?>">
                <?php echo($description) ?>
              </p>
              <p style="<?php echo($style['p-margin']) ?>">
                Our team will contact you back within 24 hours. Until then, feel free to contact us anytime
                <br/>at +66-2586-2222
                <br/>or email scginternational@scg.com.
              </p>
            </td>
          </tr>

        </table>
      </td>
    </tr>

    <!------------------------------------->
    <!------------------------------------->
    <!----- Footer ----->
    <!------------------------------------->
    <!------------------------------------->
    <tr>
      <td style="background-color: #ed1c24;">
        <table align="center" role="presentation" cellspacing="0" cellpadding="0" border="0" style="margin: auto;">
          <tr>
            <td style="<?php echo($style['footer-padding']) ?>">
              <table align="center" role="presentation" cellspacing="0" cellpadding="0" border="0" style="margin: auto;">
                <tr>
                  <td style="<?php echo($style['footer-link-wrapk']) ?>">
                    <a style="<?php echo($style['footer-link']) ?>" href="https://www.scg.com/en/01corporate_profile/">Corporate</a>
                    <a style="<?php echo($style['footer-link']) ?>" href="https://career.scg.com/en">Career</a>
                    <a style="<?php echo($style['footer-link']) ?>" href="">Policy</a>
                  </td>
                </tr>
              </table>
              <table align="center" role="presentation" cellspacing="0" cellpadding="0" border="0" style="margin: auto;">
                <tr>
                  <td style="<?php echo($style['copyright']) ?>">
                    ©2019. SCG International<br/>
                    <span style="color: #ffffff !important; text-decoration: none;">1 Siam Cement Road, Bangsue, Bangkok 10800 Thailand</span>
                  </td>
                </tr>
              </table>
            </td>
          </tr>
        </table>
      </td>
    </tr>

  </table>
  <!-- Email Body : END -->

  <!--[if mso | IE]>
  </td>
  </tr>
  </table>
  <![endif]-->
</center>
