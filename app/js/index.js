import SlideNav from './slideNav.es6'
import { disableBodyScroll, clearAllBodyScrollLocks } from './bodyScrollLock.es6'

window.onload = function (e) {
  document.getElementsByTagName("BODY")[0].classList.add("window-loaded");
};

$(document).ready(function () {

  var SLICK_SETTINGS = {
    slidesToShow: 1,
    arrows: true,
    autoplay: true,
    autoplaySpeed: 3000,
    prevArrow: '<div class="prev-arrow"></div>',
    nextArrow: '<div class="next-arrow"></div>',
  };

  var SCROLL_SETTINGS = {
    slidesToShow: 1,
    arrows: true,
    autoplay: true,
    autoplaySpeed: 3000,
    prevArrow: '<div class="prev-arrow"></div>',
    nextArrow: '<div class="next-arrow"></div>',
  };

  popup('popup-policy', 'pop-policy');
  popup('popup-news', 'pop-news');
  popup('popup-contact', 'pop-contact');

  initNavMenu();
  initForm();
  initHomeContactButton();
  initIntro();
  initParticle();
  initVideo();

  sectionNews();
  sectionProducts();
  sectionPartners();

  // Scroll
  var position = null;
  var scrollCount = 0;
  var debouceRemoveScrolling = debounce(function () {
    scrollCount = 0;
    $('body').removeClass('scrolling');
  }, 1500);
  $(window).scroll(function () {
    var scroll = $(window).scrollTop();
    if (scroll !== position && position != null) {
      scrollCount += Math.abs(scroll - position);
      if (scrollCount > 300) {
        $('body').addClass('scrolling');
      }
      debouceRemoveScrolling();
    }
    position = scroll;
  });

  // Windows resize (get width, height, ratio mostly)
  $(window).resize(function () {

    // Home Section - to contain content within
    // ----------------------------------------
    var $nav = $('#main-nav');
    var winWidth, winHeight;
    // use element dimensions to prevent stutter from mobile browser
    if (mobileAndTabletcheck()) {
      winWidth = $('#_sec-main').width();
      winHeight = $('#_sec-main').height() - $nav.height() - 50;
    } else {
      winWidth = $(window).width();
      winHeight = $(window).height() - $nav.height() - 50;
    }
    var $el = $('#intro-wrap');
    /**
     * Just for mobile performance
     * Already disable 'transform' in the CSS
     */
    if (winWidth > 991) {
      var scale = Math.min(
        winWidth / $el.outerWidth(),
        winHeight / $el.outerHeight(),
        1
      );
      $el.css({
        transform: "translate(-50%, 0) " + "scale(" + scale + ")"
      });
    }


  });
  // trigger resize event
  if (navigator.userAgent.indexOf('MSIE') !== -1 || navigator.appVersion.indexOf('Trident/') > 0) {
    var evt = document.createEvent('UIEvents');
    evt.initUIEvent('resize', true, false, window, 0);
    window.dispatchEvent(evt);
  } else {
    window.dispatchEvent(new Event('resize'));
  }
  /**
   * CSS use 100vh height first then this override the height
   */
  setTimeout(function () {
    if (mobileAndTabletcheck()) {
      var viewportHeight = $('#_sec-main').outerHeight();
      $('#_sec-main').css({ height: viewportHeight });
    }
  }, 500);

  // Back to top
  $('#back-to-top').click(function () {
    var duration = $(document).scrollTop() || 500;
    $('html, body').animate({ scrollTop: 0 }, { duration: parseInt(duration) / 2 });
  });


  function initNavMenu () {

    $('#nav-toggle').click(function () {
      $('body').toggleClass('mobile-menu-expanded');
      if ($('body').hasClass('mobile-menu-expanded')) {
        disableBodyScroll(document.querySelector("#nav-expanded"));
      } else {
        clearAllBodyScrollLocks()
      }
    });
    $('#nav-expanded .nav-item').click(function () {
      $('body').removeClass('mobile-menu-expanded');
      clearAllBodyScrollLocks()
    });

    window.onscroll = function () {
      scrollFunction()
    };
    // Scroll to section
    window.slide = new SlideNav();
    var nav = new SlideNav({
      activeClass: "active",
      toggleButtonSelector: true,
      changeHash: true,
    });
  }

  function scrollFunction () {
    // Sticky header & back to top
    if (document.body.scrollTop > 100 || document.documentElement.scrollTop > 100) {
      document.body.classList.add('scrolled-nav');
      if (document.body.scrollTop > 500 || document.documentElement.scrollTop > 500) {
        document.body.classList.add('scrolled-far');
      } else {
        document.body.classList.remove('scrolled-far');
      }
      $('#back-to-top').removeClass('hidden')
    } else {
      document.body.classList.remove('scrolled-nav');

      $('#back-to-top').addClass('hidden')
    }
  }

  function popup (modalId, triggerClass) {
    var modal = document.getElementById(modalId);
    var closes = document.getElementsByClassName('close-popup');
    var triggers = document.getElementsByClassName(triggerClass);

    // When the user clicks on the button, open the modal
    for (var i = 0; i < triggers.length; i++) {
      triggers[i].onclick = function () {
        modal.style.display = "block";
        document.body.classList.add('body-no-scroll');
        var ps = new PerfectScrollbar('#' + modalId + ' .scroll-container', SCROLL_SETTINGS);
        disableBodyScroll(document.querySelector("#" + modalId));

        /**
         * There are different types of popups
         * Below are popup's specific operations.
         */
        if (triggerClass === 'pop-contact') {
          $(modal).removeClass(['email-sent', 'sending']);
        }
      };
    }
    // fix for iPhone iPad
    for (var i = 0; i < closes.length; i++) {
      closes[i].onclick = function () {
        $('.ml-popup').css('display', 'none');
        // modal.style.display = "none";
        document.body.classList.remove('body-no-scroll');
        clearAllBodyScrollLocks()
      }
    }
  }

  function initForm () {
    $('.ml-input').focus(function () {
      $(this).parent().addClass('focus')
    }).blur(function () {
      $(this).parent().removeClass('focus')
    }).change(function () {
      if (this.value === '') {
        $(this).parent().removeClass('has-value')
      } else {
        $(this).parent().addClass('has-value')
      }
    });
  }

  function openPopupById (modalId) {
    var modal = document.getElementById(modalId);
    modal.style.display = "block";
    document.body.classList.add('body-no-scroll');
    new PerfectScrollbar('#' + modalId + ' .scroll-container', SCROLL_SETTINGS);
    disableBodyScroll(document.querySelector("#" + modalId));
  }

  function sectionNews () {
    $.ajax({
      url: "./data/news.php",
      dataType: "json",
    }).done(function (data) {
      var $news = $('#news-carousel').first();
      $.each(data, function (key, value) {
        $news.append(
          '<div class="news-item" data-news-index="' + key + '">' +
          '<div class="content-wrap">' +
          '<div class="_thumb" style="background-image: url(' + value.thumb + ')"></div>' +
          '<p class="_category">' + value.category + '</p>' +
          '<p class="_date">' + value.posted_at + '</p>' +
          '<p class="_title">' + value.title + '</p>' +
          '</div>' +
          '</div>'
        )
      });

      $news.slick({
        // centerMode: true,
        // centerPadding: '20%',
        slidesToShow: 3,
        arrows: true,
        prevArrow: '<div class="prev-arrow"></div>',
        nextArrow: '<div class="next-arrow"></div>',
        infinite: false,
        autoplay: false,
        responsive: [
          {
            breakpoint: 1200,
            settings: {
              slidesToShow: 2,
            }
          },
          {
            breakpoint: 575,
            settings: {
              slidesToShow: 1,
            }
          },
        ],
      }).on("click", function (e) {
        var index = $(e.target).parents('.news-item').data("news-index");
        if (index === undefined) {
          return
        }
        $.ajax({
          url: "./data/news.php?index=" + index,
          dataType: "json",
        }).done(function (data) {
          if (data) {
            openPopupById('popup-news');

            var $thisPopup = $('#popup-news').first();

            $thisPopup.find('._content').html(data.content);
            $thisPopup.find('._header-1').html(data.title);
            $thisPopup.find('._category').html(data.category);
            $thisPopup.find('._date').html(data.posted_at);

            // scroll
            new PerfectScrollbar('#popup-news .scroll-container', SCROLL_SETTINGS);
            new PerfectScrollbar('#popup-news .all-content-wrap', SCROLL_SETTINGS);

            // carousel
            var $carouselItems = [];
            $.each(data.images, function (key, value) {
              $carouselItems.push('<div class="_carousel_item" style="background-image: url(' + value + ')"</div>');
            });
            var $currentSlick = $thisPopup.find('.popup-carousel');
            if ($currentSlick.hasClass('slick-initialized')) {
              $currentSlick.slick('destroy');
            }
            $currentSlick.html($carouselItems);
            $currentSlick.slick(SLICK_SETTINGS);
          }

        });
      }).on('afterChange', function(event, slick, currentSlide) {
        // console.log(currentSlide);
        // console.log('-', slick.options.slidesToShow);
        //If we're on the first slide hide the Previous button and show the Next
        if (currentSlide === 0) {
          $('.prev-arrow', $news).css({ opacity: 0});
          $('.next-arrow', $news).css({ opacity: 1});
        }
        else {
          $('.prev-arrow', $news).css({ opacity: 1});
        }

        //If we're on the last slide hide the Next button.
        if ((slick.slideCount - slick.options.slidesToShow) === currentSlide) {
          $('.next-arrow', $news).css({ opacity: 0});
        }
      });
      // hidden by default
      $('.prev-arrow', $news).css({ opacity: 0});

    });

  }

  function sectionProducts () {
    $.ajax({
      url: "./data/products.php",
      dataType: "json",
    }).done(function (data) {
      var $products = $('#product-carousel').first();
      $.each(data, function (key, value) {
        $products.append(
          '<div class="product-item" data-index="' + key + '">' +
          '<div class="content-wrap">' +
          '<div class="_thumb" style="background-image: url(' + value.thumb + ')"></div>' +
          '<p class="_title">' + value.title + '</p>' +
          '<p class="_excerpt">' + value.excerpt + '</p>' +
          '</div>' +
          '</div>'
        )
      });

      $products.slick({
        centerMode: true,
        centerPadding: '31%',
        slidesToShow: 1,
        initialSlide: 1, // 1 for center
        arrows: false,
        infinite: true,
        // autoplay: false,
        // autoplaySpeed: 5000,
        pauseOnHover: false,
        speed: 500,
        responsive: [
          {
            breakpoint: 991,
            settings: {
              centerPadding: '31%',
            }
          },
        ],
      })
        .on("click", function (e) {
          var index = $(e.target).parents('.slick-slide').data("slick-index");
          $products.slick('slickGoTo', index);
          // $('.slick-slider').slick('slickNext');
        })
      // No autoplay
      // .on("mouseover", function (e) {
      //   if ($(e.target).parents('.slick-slide').hasClass('slick-center')) {
      //     $products.slick('slickPause');
      //   } else {
      //     $products.slick('slickPlay');
      //   }
      // })
      // .on("mouseleave", function (e) {
      //   $products.slick('slickPlay');
      // });

    });

  }

  function sectionPartners () {
    var $partners = $('.partner-wrap').first();

    $partners.children().each(function (i, val) {
      $(this).wrap('<div class="_wrapper"></div>')
    });
    $partners.slick({
      // slidesToShow: 2,
      slidesPerRow: 6,
      rows: 3,
      arrows: false,
      infinite: true,
      autoplay: true,
      autoplaySpeed: 6000,
      pauseOnHover: false,
      speed: 500,
      dots: true,
      dotsClass: 'custom-dots', //slick generates this <ul.custom-dots> within the appendDots container
      customPaging: function (slider, i) {
        //FYI just have a look at the object to find available information
        //press f12 to access the console
        //you could also debug or look in the source
        var slideNumber = (i + 1),
          totalSlides = slider.slideCount;
        return '<div class="dot-wrap">' +
          '<div class="hex outer-dot"><div class="top"></div><div class="middle"></div><div class="bottom"></div></div>' +
          '<div class="hex inner-dot"><div class="top"></div><div class="middle"></div><div class="bottom"></div></div>' +
          '</div>';
      },
      responsive: [
        {
          breakpoint: 991,
          settings: {
            slidesPerRow: 3,
            rows: 6,
          }
        },
      ],
    })
  }

  function debounce (func, wait, immediate) {
    var timeout;
    return function () {
      var context = this, args = arguments;
      var later = function () {
        timeout = null;
        if (!immediate) func.apply(context, args);
      };
      var callNow = immediate && !timeout;
      clearTimeout(timeout);
      timeout = setTimeout(later, wait);
      if (callNow) func.apply(context, args);
    };
  }

  function initHomeContactButton () {
    var $input = $('#home-email-input');

    // touch
    if (mobileAndTabletcheck()) {
      $(document)
        .on("touchstart", '#home-contact-btn', function (event) {
          $(this).addClass('active');
          event.stopPropagation();
        })
        .on("touchstart", '#_sec-main', function (event) {
          $('#home-contact-btn').removeClass('active');
          event.stopPropagation();
        });
    }
    // mouse enter
    else {
      var onMouseLeave = debounce(function () {
        $('#home-contact-btn').removeClass('active');
      }, 1000);
      $('#home-contact-btn').mouseenter(function () {
        if (!mobileAndTabletcheck()) {
          $(this).addClass('active');
        }
      }).mouseleave(onMouseLeave);
    }

    $input.keyup(function (e) {
      if (e.keyCode === 13) {
        if ($(this).val()) {

          openContactPopup($(this).val());
        }
      }
    });
    $('#home-contact-btn .btn-primary').click(function () {
      if ($input.val()) {
        openContactPopup($('#home-email-input').val());
      }
    });
  }

  function openContactPopup (email) {
    openPopupById('popup-contact');
    $('#popup-contact #email').val(email).parent().addClass('has-value');
    $('#popup-contact #name').focus()
  }

  function initIntro () {
    /**
     * Note: Intro is changed to loading
     */
    // $('#sec-intro .desc-wrap').slick({
    //   slidesToShow: 1,
    //   initialSlide: 1,
    //   arrows: false,
    //   infinite: true,
    //   autoplaySpeed: 3000,
    //   autoplay: true,
    // });

    // Cookie
    // var CNAME = 'intro';
    // console.log('cookie',getCookie(CNAME) );
  }

  function setCookie (cname, cvalue) {
    var exdays = 7;
    var d = new Date();
    d.setTime(d.getTime() + (exdays * 24 * 60 * 60 * 1000));
    var expires = "expires=" + d.toGMTString();
    document.cookie = cname + "=" + cvalue + "; " + expires;
  }

  function getCookie (cname) {
    var name = cname + "=";
    var ca = document.cookie.split(';');
    for (var i = 0; i < ca.length; i++) {
      var c = ca[i].trim();
      if (c.indexOf(name) == 0) return c.substring(name.length, c.length);
    }
    return "";
  }

  function initParticle () {
    var SETTINGS = {
      "particles": {
        "number": {
          "value": 80,
          "density": {
            "enable": true,
            "value_area": 800
          }
        },
        "color": {
          "value": "#8c8c8c"
        },
        "shape": {
          "type": "circle",
          "stroke": {
            "width": 0,
            "color": "#000000"
          },
          "polygon": {
            "nb_sides": 5
          },
          "image": {
            "src": "img/github.svg",
            "width": 100,
            "height": 100
          }
        },
        "opacity": {
          "value": 0.5,
          "random": false,
          "anim": {
            "enable": false,
            "speed": 1,
            "opacity_min": 0.1,
            "sync": false
          }
        },
        "size": {
          "value": 1.5,
          "random": false,
          "anim": {
            "enable": false,
            "speed": 40,
            "size_min": 0.1,
            "sync": false
          }
        },
        "line_linked": {
          "enable": true,
          "distance": 150,
          "color": "#8c8c8c",
          "opacity": 0.5,
          "width": 1
        },
        "move": {
          "enable": true,
          "speed": 0.5,
          "direction": "none",
          "random": false,
          "straight": false,
          "out_mode": "out",
          "bounce": false,
          "attract": {
            "enable": false,
            "rotateX": 600,
            "rotateY": 1200
          }
        }
      },
      "interactivity": {
        "detect_on": "canvas",
        "events": {
          "onhover": {
            "enable": false,
            "mode": "repulse"
          },
          "onclick": {
            "enable": false,
            "mode": "push"
          },
          "resize": true
        },
        "modes": {
          "grab": {
            "distance": 400,
            "line_linked": {
              "opacity": 1
            }
          },
          "bubble": {
            "distance": 400,
            "size": 40,
            "duration": 2,
            "opacity": 8,
            "speed": 3
          },
          "repulse": {
            "distance": 200,
            "duration": 0.4
          },
          "push": {
            "particles_nb": 4
          },
          "remove": {
            "particles_nb": 2
          }
        }
      },
      "retina_detect": true
    };
    particlesJS('particles-product', SETTINGS);
    particlesJS('particles-principle', SETTINGS);
  }

  function initVideo () {
    $('#sec-solution .solution').hover(hoverVideo, hideVideo)
  }

  function hoverVideo (e) {
    $('video', this).get(0).play();
  }

  function hideVideo (e) {
    var me = this;
    setTimeout(function () {
      var $video = $('video', me).get(0);
      $video.pause();
      $video.currentTime = 0;
      // $video.load()
    }, 500);
  }

});

window.mobileAndTabletcheck = function () {
  var check = false;
  (function (a) {
    if (/(android|bb\d+|meego).+mobile|avantgo|bada\/|blackberry|blazer|compal|elaine|fennec|hiptop|iemobile|ip(hone|od)|iris|kindle|lge |maemo|midp|mmp|mobile.+firefox|netfront|opera m(ob|in)i|palm( os)?|phone|p(ixi|re)\/|plucker|pocket|psp|series(4|6)0|symbian|treo|up\.(browser|link)|vodafone|wap|windows ce|xda|xiino|android|ipad|playbook|silk/i.test(a) || /1207|6310|6590|3gso|4thp|50[1-6]i|770s|802s|a wa|abac|ac(er|oo|s\-)|ai(ko|rn)|al(av|ca|co)|amoi|an(ex|ny|yw)|aptu|ar(ch|go)|as(te|us)|attw|au(di|\-m|r |s )|avan|be(ck|ll|nq)|bi(lb|rd)|bl(ac|az)|br(e|v)w|bumb|bw\-(n|u)|c55\/|capi|ccwa|cdm\-|cell|chtm|cldc|cmd\-|co(mp|nd)|craw|da(it|ll|ng)|dbte|dc\-s|devi|dica|dmob|do(c|p)o|ds(12|\-d)|el(49|ai)|em(l2|ul)|er(ic|k0)|esl8|ez([4-7]0|os|wa|ze)|fetc|fly(\-|_)|g1 u|g560|gene|gf\-5|g\-mo|go(\.w|od)|gr(ad|un)|haie|hcit|hd\-(m|p|t)|hei\-|hi(pt|ta)|hp( i|ip)|hs\-c|ht(c(\-| |_|a|g|p|s|t)|tp)|hu(aw|tc)|i\-(20|go|ma)|i230|iac( |\-|\/)|ibro|idea|ig01|ikom|im1k|inno|ipaq|iris|ja(t|v)a|jbro|jemu|jigs|kddi|keji|kgt( |\/)|klon|kpt |kwc\-|kyo(c|k)|le(no|xi)|lg( g|\/(k|l|u)|50|54|\-[a-w])|libw|lynx|m1\-w|m3ga|m50\/|ma(te|ui|xo)|mc(01|21|ca)|m\-cr|me(rc|ri)|mi(o8|oa|ts)|mmef|mo(01|02|bi|de|do|t(\-| |o|v)|zz)|mt(50|p1|v )|mwbp|mywa|n10[0-2]|n20[2-3]|n30(0|2)|n50(0|2|5)|n7(0(0|1)|10)|ne((c|m)\-|on|tf|wf|wg|wt)|nok(6|i)|nzph|o2im|op(ti|wv)|oran|owg1|p800|pan(a|d|t)|pdxg|pg(13|\-([1-8]|c))|phil|pire|pl(ay|uc)|pn\-2|po(ck|rt|se)|prox|psio|pt\-g|qa\-a|qc(07|12|21|32|60|\-[2-7]|i\-)|qtek|r380|r600|raks|rim9|ro(ve|zo)|s55\/|sa(ge|ma|mm|ms|ny|va)|sc(01|h\-|oo|p\-)|sdk\/|se(c(\-|0|1)|47|mc|nd|ri)|sgh\-|shar|sie(\-|m)|sk\-0|sl(45|id)|sm(al|ar|b3|it|t5)|so(ft|ny)|sp(01|h\-|v\-|v )|sy(01|mb)|t2(18|50)|t6(00|10|18)|ta(gt|lk)|tcl\-|tdg\-|tel(i|m)|tim\-|t\-mo|to(pl|sh)|ts(70|m\-|m3|m5)|tx\-9|up(\.b|g1|si)|utst|v400|v750|veri|vi(rg|te)|vk(40|5[0-3]|\-v)|vm40|voda|vulc|vx(52|53|60|61|70|80|81|83|85|98)|w3c(\-| )|webc|whit|wi(g |nc|nw)|wmlb|wonu|x700|yas\-|your|zeto|zte\-/i.test(a.substr(0, 4))) check = true;
  })(navigator.userAgent || navigator.vendor || window.opera);
  return check;
};
