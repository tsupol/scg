$(document).ready(function () {

  setTimeout(initParallax(), 1000);

  function initParallax () {
    var controller = new ScrollMagic.Controller({ globalSceneOptions: { triggerHook: "onEnter", duration: "200%" } });
    var controller3 = new ScrollMagic.Controller({ globalSceneOptions: { triggerHook: "onEnter", duration: "100%" } });
    var controller2 = new ScrollMagic.Controller();

    var DELAY_STEP = .25;
    var DELAY_START = .15;

    var isMobile = mobileAndTabletcheck();

    // Animate On Scroll
    // ----------------------------------------

    var animateList = [
      {
        options: { triggerElement: "#_sec-counter" },
        items: [
          {
            selector: '#_sec-counter ._outer',
            animation: 'fade_slide_up',
            delay: DELAY_START + (DELAY_STEP * 0)
          },
          {
            selector: '#_sec-counter ._counter-1',
            animation: 'fade_slide_up',
            delay: DELAY_START + (DELAY_STEP * 1)
          },
          {
            selector: '#_sec-counter .bottom-wrap',
            animation: 'fade_slide_up',
            delay: DELAY_START + (DELAY_STEP * 2)
          },
        ]
      },
      // solution
      {
        options: { triggerElement: "#sec-solution" },
        items: [
          {
            selector: '#sec-solution .solution:nth-child(odd)',
            animation: 'fade_slide_up',
            delay: DELAY_START + (DELAY_STEP * 0),
            transition:
              'transform 1.5s cubic-bezier(0.19, 1, 0.22, 1) ' + (DELAY_START + (DELAY_STEP * 0)) + 's, ' +
              'opacity 1.5s cubic-bezier(0.19, 1, 0.22, 1) ' + (DELAY_START + (DELAY_STEP * 0)) + 's,' +
              'width .35s ease-in-out .1s'
          },
          {
            selector: '#sec-solution .solution:nth-child(even)',
            animation: 'fade_slide_up',
            delay: DELAY_START + (DELAY_STEP * 1),
            transition:
              'transform 1.5s cubic-bezier(0.19, 1, 0.22, 1) ' + (DELAY_START + (DELAY_STEP * 1)) + 's, ' +
              'opacity 1.5s cubic-bezier(0.19, 1, 0.22, 1) ' + (DELAY_START + (DELAY_STEP * 1)) + 's,' +
              'width .35s ease-in-out .1s'
          },
          {
            selector: '#sec-solution .color-white',
            animation: 'black_to_white',
            delay: DELAY_START
          },
        ]
      },
      // products & services
      {
        options: {
          triggerElement: "#sec-products",
          offset: isMobile ? 50 : 150
        },
        items: [
          {
            selector: '#sec-products .slick-slide:nth-child(odd)',
            animation: 'fade_slide_up',
            delay: DELAY_START + (DELAY_STEP * 0)
          },
          {
            selector: '#sec-products .slick-slide:nth-child(even)',
            animation: 'fade_slide_up',
            delay: DELAY_START + (DELAY_STEP * 1)
          },
          // {
          //   selector: '#sec-products .slick-slide',
          //   animation: 'fade_slide_up',
          //   delay: DELAY_START + (DELAY_STEP * 2)
          // },
        ]
      },
      // news
      {
        options: {
          triggerElement: "#sec-news",
          offset: isMobile ? 50 : 100
        },
        items: [
          {
            selector: '#sec-news .slick-slide:nth-child(1)',
            animation: 'fade_slide_up',
            delay: DELAY_START + (DELAY_STEP * 0)
          },
          {
            selector: '#sec-news .slick-slide:nth-child(2)',
            animation: 'fade_slide_up',
            delay: DELAY_START + (DELAY_STEP * 1)
          },
          {
            selector: '#sec-news .slick-slide:nth-child(3)',
            animation: 'fade_slide_up',
            delay: DELAY_START + (DELAY_STEP * 2)
          },
          {
            selector: '#sec-news .slick-slide:nth-child(4)',
            animation: 'fade_slide_up',
            delay: DELAY_START + (DELAY_STEP * 3)
          },
        ]
      },
    ];
    var i, j;
    for (i = 0; i < animateList.length; i++) {
      let scene = animateList[i];
      for (j = 0; j < scene.items.length; j++) {
        let item = scene.items[j];
        $(item.selector).css(getAnimationCSS(item));
      }
      new ScrollMagic.Scene(scene.options)
        .on('enter leave', function (e) {
          for (j = 0; j < scene.items.length; j++) {
            let item = scene.items[j];
            $(item.selector).css(getAnimationCSS(item, e.type));
          }

          // customization
          if($(e.target.triggerElement()).attr('id') === 'sec-products') {
            setProductCarousel()
          }

        })
        // .addIndicators()
        .addTo(controller2);
    }

    // Parallax
    // ----------------------------------------

    var list = [
      // ['#mesh-principle', '#_sec-principle', 39, -80, 'top', true],
      // ['#mesh-product', '#_sec-partner', 60, 50, 'bottom'],
      ['#particles-principle', '#_sec-principle', 40, 30, 'top', false],
      ['#particles-product', '#_sec-partner', 70, 40, 'bottom'],
      ['#bg-text-products', '#sec-products', 23, 33, 'top'],
      ['#text-community', '#_sec-community', 5, -60, 'top'],
      // ['#bg-text-news', '#sec-news', 0, 30, 'top'],
      // Note: low performance for images
      // ['#bg-sec-community', '#_sec-community', 15, 20, 'top'],
    ];
    for (i = 0; i < list.length; i++) {
      let item = list[i];
      if (isMobile && !item[5]) continue;
      var attr = item[4] || 'top';
      $(item[0]).css(attr, (item[2] - (item[3] / 3)) + 'vw');
      new ScrollMagic.Scene({ triggerElement: item[1] })
        .setTween(item[0], {
          y: item[3] + "vw", ease: Linear.easeNone
        })
        // .addIndicators()
        .addTo(controller);
    }

    new ScrollMagic.Scene({
      triggerElement: "#_sec-principle",
      offset: -150
    })
      .setClassToggle('#_sec-principle .principle', 'do-animate')
      // .addIndicators()
      .addTo(controller2);

    new ScrollMagic.Scene({
      triggerElement: "#_sec-counter",
      // offset: -250
    })
      .setTween("#_sec-counter", { opacity: 1 })
      .on('start leave', function (e) {
        if (e.type === 'start') {
          initCounter()
        }
      })
      // .addIndicators()
      .addTo(controller2);

    // Activity - prevent duration longer than footer
    // ----------------------------------------
    // ['#bg-text-news', '#sec-news', 0, 30, 'top'],
    if (!isMobile) {
      new ScrollMagic.Scene({ triggerElement: '#sec-news' })
        .setTween('#bg-text-news', {
          y: 10 + "vw", ease: Linear.easeNone
        })
        // .addIndicators()
        .addTo(controller3);
    }

    // Map
    // ----------------------------------------
    new ScrollMagic.Scene({
      triggerElement: "#sec-presences",
      offset: isMobile ? 100 : 250
    })
      .on('start leave', function (e) {
        if (e.type === 'start') {
          $('#world-map').addClass('do-animate');
          $('#sec-presences .stat-wrap').addClass('do-animate');
          $('#exploding-line').addClass('explode');
        } else if (e.type === 'leave') {
          $('#sec-presences .do-animate').removeClass('do-animate');
          $('#exploding-line').removeClass('explode');
        }
      })
      // .addIndicators()
      .addTo(controller2);

  }

  function getAnimationCSS (item, eventType = 'leave') {
    if (item.animation === 'fade_slide_up') {
      if (eventType === 'enter') {
        return {
          transform: 'translateY(0)',
          opacity: 1,
          transition: item.transition || 'all 1.5s cubic-bezier(0.19, 1, 0.22, 1) ' + item.delay + 's'
        }
      } else if (eventType === 'leave') {
        return {
          transform: 'translateY(20vw)',
          opacity: 0,
          transition: 'all .5s ease-out'
        }
      }
    }
    else if (item.animation === 'black_to_white') {
      if (eventType === 'enter') {
        return {
          color: '#ffffff',
          transition: 'all 1.5s cubic-bezier(0.19, 1, 0.22, 1) ' + item.delay + 's'
        }
      } else if (eventType === 'leave') {
        return {
          color: '#000000',
          transition: 'all 1.15s cubic-bezier(0.19, 1, 0.22, 1)'
        }
      }
    }
  }

  function initCounter () {
    var comma_separator = $.animateNumber.numberStepFactories.separator(',');
    var easing = 'easeOutQuint';
    // setTimeout(function () {
    $('.run-number-partner').prop('number', 0).animateNumber({
      number: 2000,
      numberStep: comma_separator
    }, 2000, easing);
    $('.run-number-trade').prop('number', 0).animateNumber({
      number: 32000,
      numberStep: comma_separator
    }, 2000, easing);
    $('.run-number-turnover').animateNumber({
      number: 1.5,
      numberStep: function (now, tween) {
        // console.log('-',now)
        var target = $(tween.elem);
        target.text(now.toFixed(1));
      }
    }, 2000, easing);
    // }, 500)
  }

  function setProductCarousel () {
    /**
     * Note: for autoplay only
     */
    // var $carousel = $('#product-carousel');
    // // console.log('----', $carousel.slick('slickGetOption', 'autoplay'));
    // if(!$carousel.slick('slickGetOption', 'autoplay')) {
    //   $carousel.slick('slickSetOption', 'autoplay', true, true)
    // }
  }

});

