<!-- Pre Footer -->
<div id="pre-footer" class="layout-outer">
  <div class="_pf-text">
    <span class="hide-mdp">IF YOU NEED MORE INFORMATION OR SERVICE,</span>
    PLEASE CALL
  </div>
  <a href="tel:+6625862251" class="_pf-number">+66-2586-2222</a>
</div>

<!-- Footer -->
<div id="footer" class="layout-outer">
  <div class="_diagonal-bg"></div>
  <div class="layout-inner">
    <div class="content-wrap">
      <div class="logo-wrap flex-center">
        <img class="_footer-logo" src="./imgs/logo-white@2x.png"/>
      </div>
      <div class="bottom-wrap">
        <a href="https://www.scg.com/en/01corporate_profile/" target="_blank" class="footer-item">CORPORATE</a>
        <a href="https://career.scg.com/en" target="_blank" class="footer-item">CAREER</a>
        <a class="footer-item pop-policy">POLICY</a>
        <div class="footer-item __copyright">Copyright 2018. SCG International Corporation Co., LTD. All Right Reserved</div>
      </div>
    </div>
  </div>
</div>

<div id="back-to-top" class="bottom-btn flex-col-center"></div>
<div id="get-in-touch" class="bottom-btn flex-col-center pop-contact"></div>
