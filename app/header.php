<div id="main-nav">
  <div class="nav-bg"></div>
  <div class="nav-max">
    <div class="nav-logo"></div>
    <div class="nav-list">
      <a class="nav-item" href="#sec-home">Home</a>
      <a class="nav-item" href="https://www.scg.com/en/01corporate_profile/" target="_blank">CORPORATE</a>
      <a class="nav-item" href="#sec-solution">Services and Solutions</a>
      <a class="nav-item" href="#sec-presences">Our Presences</a>
      <a class="nav-item" href="#sec-products">Products</a>
      <a class="nav-item" href="#sec-news">News</a>
      <a class="nav-item" href="#sec-contact">Contact Us</a>
    </div>
  </div>
  <!--  mobile -->
  <div class="nav-mini">
    <div class="nav-logo"></div>
    <div class="_nav-right">
      <div id="nav-toggle"><span></span></div>
      <div class="nav-txt-menu">
        <div class="_txt-open">menu</div>
        <div class="_txt-close">close</div>
      </div>
    </div>
  </div>
</div>

<!--  expanded -->
<div id="nav-expanded">
  <div class="_inner">
    <a class="nav-item" href="#sec-home">HOME</a>
    <a class="nav-item" href="#sec-solution">SERVICES AND SOLUTIONS</a>
    <a class="nav-item" href="#sec-presences">OUR PRESENCES</a>
    <a class="nav-item" href="#sec-products">PRODUCTS</a>
    <a class="nav-item" href="#sec-news">NEWS</a>
    <a class="nav-item" href="#sec-contact">CONTACT US</a>
    <a class="nav-item" href="https://www.scg.com/en/01corporate_profile/">CORPORATE</a>
    <a class="nav-item" href="https://career.scg.com/en">CAREER</a>
    <a class="nav-item">POLICY</a>
    <div class="_btn-contact pop-contact">GET IN TOUCH</div>
  </div>
</div>
