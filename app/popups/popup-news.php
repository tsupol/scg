<!-- Popup -->
<div id="popup-news" class="ml-popup __popup-with-carousel">
  <div class="back-drop close-popup"></div>
  <div class="mp-outer">
    <div class="mp-inner">
      <div class="sp-close close-popup">×</div>

      <div class="all-content-wrap">

        <!-- Carousel -->
        <div class="popup-carousel">
        </div>

        <div class="scroll-container-wrap">
          <div class="bottom-gradient"></div>
          <div class="scroll-container">

            <!-- Begin Content (AJAX Loaded) -->
            <h1 class="_header-1"></h1>
            <p class="_category"></p>
            <p class="_date"></p>
            <div class="_content"></div>

            <br/>
            <br/>
            <br/>
            <br/>
            <!-- End Content -->
          </div>
        </div>
      </div>

    </div>
  </div>

</div>

