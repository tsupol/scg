<!-- Popup -->
<div id="popup-contact" class="ml-popup">
  <div class="back-drop close-popup"></div>
  <div class="mp-outer">
    <div class="mp-inner">
      <div class="sp-close close-popup">×</div>

      <div class="scroll-container">
        <h2 class="_heading text-center">GET IN TOUCH</h2>
        <h3 class="_subheading text-center">
          Leverage your business by being partner with us,<br/>
          SCG International Corporation Co.,Ltd.
        </h3>

        <!-- Thank you message -->
        <div class="_thank-you text-center">
          <h3>THANK YOU</h3>
          <p>We have successfully received your message.<br/> Our team will contact you shortly.</p>
        </div>

        <!-- Form -->
        <div class="form-wrap">
          <form id="contact-form" class="ml-form" action="./" method="POST">
            <!-- left col -->
            <div class="col-12 xs-col-6 _col">
              <div class="col-12 _col">
                <div class="form-item">
                  <input class="ml-input" name="email" id="email" type="email">
                  <label class="label" for="email">EMAIL</label>
                </div>
              </div>
              <div class="col-12 _col">
                <div class="form-item">
                  <input class="ml-input" name="name" id="name" type="text">
                  <label class="label" for="name">NAME</label>
                </div>
              </div>
              <div class="col-12 _col">
                <div class="form-item">
                  <input class="ml-input" name="company_name" id="company_name" type="text">
                  <label class="label" for="company_name">COMPANY NAME</label>
                </div>
              </div>
            </div>
            <!-- right col -->
            <div class="col-12 xs-col-6 _col">
              <div class="col-12 _col">
                <div class="form-item">
                  <input class="ml-input" name="tel" id="tel" type="text">
                  <label class="label" for="tel">TEL</label>
                </div>
              </div>
              <div class="col-12 _col">
                <div class="form-item">
                  <textarea class="ml-input" rows="5" cols="50" name="description" id="description"></textarea>
                  <label class="label" for="description">DESCRIPTION</label>
                </div>
              </div>
            </div>

            <!-- button -->
            <div class="col-12 xs-col-12 _col flex-center">
              <button type="submit" class="_submit-btn btn-primary">
                <div class="_sending">SENDING...</div>
                <div class="_send">SEND</div>
              </button>
            </div>
          </form>
        </div>

      </div>

    </div>
  </div>

</div>
<script>
  $(function () {
    var request;
    var validator = $('#contact-form').validate({
      rules: {
        name: { required: true },
        email: {
          required: true,
          email: true,
        },
        tel: { required: true },
        // company_name: { required: true },
        description: { required: true },
      },
      messages: {
        name: { required: 'This field is required' },
        email: { required: 'This field is required' },
        tel: { required: 'This field is required' },
        // company_name: { required: 'This field is required' },
        description: { required: 'This field is required' },
      },
      // send email
      submitHandler: function (form) {
        // Abort any pending request
        if (request) {
          request.abort();
        }
        // setup some local variables
        var $form = $(form);
        $('#popup-contact').addClass('sending');

        // Let's select and cache all the fields
        var $inputs = $form.find("input, select, button, textarea");

        // Serialize the data in the form
        var serializedData = $form.serialize();

        $inputs.prop("disabled", true);

        // Fire off the request to /form.php
        request = $.ajax({
          url: "./gmail.php",
          type: "post",
          data: serializedData,
          timeout: 15000
        });

        // Callback handler that will be called on success
        request.done(function (response, textStatus, jqXHR) {
          $('.ml-input').val('');
          $('#popup-contact').addClass('email-sent').removeClass('sending');
          var $scroll = $('#popup-contact .scroll-container');
          if($scroll && $scroll[0]) $scroll[0].scrollTop = 0;
        });

        // Callback handler that will be called on failure
        request.fail(function (jqXHR, textStatus, errorThrown) {
          alert("Something went wrong. Please try again in 10 minutes.");
          // Log the error to the console
          console.error(
            "The following error occurred: " +
            textStatus, errorThrown
          );
        });

        // Callback handler that will be called regardless
        // if the request failed or succeeded
        request.always(function () {
          // Reenable the inputs
          $inputs.prop("disabled", false);
        });
      }
    });
    $('#clear-btn').click(function () {
      $('.ml-input').val('').parent().removeClass('has-value');
      validator.resetForm();
    })
  });
</script>
