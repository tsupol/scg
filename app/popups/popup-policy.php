<!-- Popup -->
<div id="popup-policy" class="ml-popup">
  <div class="back-drop close-popup"></div>
  <div class="mp-outer">
    <div class="mp-inner">
      <div class="sp-close close-popup">×</div>
      <!-- Begin Content -->
      <!-- Note: the class `step2` is for switching to newsletters selection -->
      <h2 class="heading2 __title-policy">POLICY</h2>
      <div class="scroll-container-wrap">
        <div class="bottom-gradient"></div>
        <div class="scroll-container">
          <h4>SAFETY AND ENVIRONMENT POLICY</h4>
          <ol>
            <li>
              The company shall continuously develop management systems of environment, safety,
              occupational health and work environment to comply with laws, international standards and other
              requirements. The company shall apply those regulations to create culture in workplace.
            </li>
            <li>
              The company shall operate business in accordance with SCG&#39;s sustainable development principles,
              creating the balance among economic, social and environmental aspects and perform work under
              SCG&#39;s safety principles and SCG’s life saving rules.
            </li>
            <li>
              The company shall conduct risks and environmental assessment in order to effectively prevent
              and resolve any hazards that will affect to the safety and health of employees, contractors and
              surrounding communities or environmental pollution.
            </li>
            <li>
              The company shall continuously improve the efficiency of natural resources utilization and take
              steps toward reducing waste under the principles of reduce, reuse/recycle, and replenish (3R).
            </li>
            <li>
              The company shall promote safe workplace, prevent fire and other potential hazards that could
              impact to employees, contractors, communities and surrounding areas.
            </li>
            <li>
              The company shall promote the involvements and suggestions in the area of environment,
              occupational health, safety and work environment. In addition, consultation between the company
              and contractors/staff should be opened to minimize risks from operations.
            </li>
            <li>
              The company shall allocate resources, tools and equipment needed for the operation so that
              employees and contractors can perform work safely.
            </li>
          </ol>

          <h4>TRADE COMPLIANCE AND CARGO SECURITY POLICY</h4>
          <p>
            SCG International is committed to complying with all import, export, and trade compliance laws and
            regulations of all countries which it operates its businesses. These trade compliance and cargo
            security policies reflects our commitment to operate in accordance with SCG’s corporate governance
            principles and the code of conduct, business code of ethics, and international norms and practices.
            All SCG International management, employees, outsourced service providers, business partners shall
            be communicated, made aware, and adhere strictly to the policies stated below:
            In conducting its businesses, SCG International Co., Ltd., its affiliated companies, and its outsourced
            service providers shall strictly comply with trade regulations, national import and export control laws
            and regulations, UN Security Council Resolutions (UNSCR), World Customs Organization (WCO)
            regulations, and other international nonproliferation norms including trade embargoes and
            antidumping regulations.
          </p>
          <p>
            All imports and exports are to be accurately declared. All shipping documents shall accurately
            describe the item(s), quantities, and value being shipped, as well as the applicable item classification
            and the country of origin.
            SCG International’s products that are classified as dual-use items shall be controlled to ensure that
            they are not used for the production of weapons of mass destruction (WMD). Verifications shall be
            made to ensure that these products are not used for purposes other than their declared use.
            SCG International, through its internal compliance program (ICP), shall verify its suppliers,
            contractors, freight forwarders, warehouse operators, customs agents, distribution channels,
            business partners, and customers to ensure that they are not affiliated to developers, producers, or
            manufacturers of weapons of mass destruction (WMD)
          </p>
          <p>
            SCG International Co., Ltd. and its affiliated companies shall manage the security of its supply chain
            in order to ensure that all its imported and exported products are free from possible security threats
            while in-transit and/or storage via the use of locks and tamper-proof seals, inspection and/or
            validation of cargo, information security management system, physical security, access control, and
            the verification of all related parties and personnel involved in the supply chain.
            Records of trade transactions shall be maintained for the duration as specified by laws and
            regulations
          </p>
          <p>
            SCG International employees shall be trained and made aware of its export and import control
            obligations to minimize the risk of accidental violations
            Any violation or suspected violation to the above policy, and/or cargo security threats shall be
            immediately reported to the Managing Director of SCG International Co., Ltd. Remedial corrective
            action(s) to the violation and/or threats in accordance with SCG International’s business continuity
            management (BCM) procedure shall be taken without delay.

          <h4>QUALITY POLICY</h4>

          <p>In order to actualize SCG International’s vision of becoming ‘The most trusted business partner’ the
            management of SCG International has established a Quality Policy as follows</p>
          <ol>
            <li>
              SCG International shall deliver products and services that match with customers’ requirements,
              both in terms of quality and delivery.
            </li>
            <li>
              SCG International shall conduct its businesses in a sustainable manner, complying with all related
              laws and regulations, as well as SCG’s codes of conduct and corporate governance principles.
            </li>
            <li>
              All products and services of SCG International shall be proven safe, free from risks of any possible
              injury or damage to our customers.
            </li>
            <li>
              All SCG International’s staff shall be responsive to all stakeholders’ enquiries and feedbacks. All
              customer claims/complaints shall be resolved with the utmost urgency, and in an effective manner.
            </li>
            <li>
              SCG International shall continually improve its processes in order to assure the quality of our
              products and services.
            </li>
            <li>
              All SCG International’s staff shall foster organizational learning through actively sharing knowledge
              into SCG International’s knowledge management system.
            </li>
          </ol>
          <br/>
          <br/>
        </div>
      </div>
    </div>
    <!-- End Content -->
  </div>

</div>
