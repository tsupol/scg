<?php
$asset_path = "./";
?>
<!DOCTYPE html>
<html>
<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, user-scalable=0">
  <meta name="description" content="SCG International, Expertise in complete services and solutions with international presences and strong alliances.">
  <title>SCG</title>
  <link rel="canonical" href="http://scg.artplore.com"/>
  <link rel="stylesheet" href="fonts/helvetica_neue/stylesheet.css" type="text/css" media="all"/>
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/slick-carousel/1.9.0/slick.min.css" type="text/css" media="all"/>
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/jquery.perfect-scrollbar/1.4.0/css/perfect-scrollbar.min.css" type="text/css" media="all"/>
  <link rel="stylesheet" type="text/css" media="all"
        href="<?php echo $asset_path ?>css/main.css?<?php echo time(); ?>"/>
  <meta name="robots" content="noindex"/>
  <style>
    body {
      /*height: 100vh;*/
      overflow: hidden;
    }

    body.window-loaded {
      background-color: #ffffff;
      height: auto;
      overflow: auto;
    }

    body.window-loaded #sec-intro {
      opacity: 0;
      height: 0;
    }

  </style>
</head>
<body>

<!--<div id="sec-loading">-->
<!--  <div class="self-center">-->
<!--    <div class="hexagon"></div>-->
<!--  </div>-->
<!--</div>-->

<div id="sec-intro">
  <img class="_bi-scg" src="./imgs/banner-intro.jpg" style="display: none"/>
  <div class="_intro">
    <div class="desc-wrap">
      <div class="horizontal-wrap">
        <div class="_desc-item">
          customer centric<br/>
          data driven<br/>
          services and solutions
        </div>
        <div class="_desc-item">
          services and solutions<br/>
          strong alliance<br/>
          international presences
        </div>
      </div>
      <!-- Note: I have to duplicate the content for css-only infinite scroll effect -->
      <div class="horizontal-wrap second-one">
        <div class="_desc-item">
          customer centric<br/>
          data driven<br/>
          services and solutions
        </div>
        <div class="_desc-item">
          services and solutions<br/>
          strong alliance<br/>
          international presences
        </div>
      </div>
    </div>
  </div>
</div>

<?php include "./header.php" ?>

<div id="sec-home">

  <div id="_sec-main">
    <video id="home-video" autobuffer playsinline autoplay loop muted>
      <source src="./videos/home-video-480p-70.mp4" type="video/mp4">
      <source src="./videos/home-video-480p.webm" type="video/webm">
      Your browser does not support the video tag.
    </video>
    <div class="home-video-overlay"></div>

    <div id="intro-wrap">
      <img class="_bi-scg" src="./imgs/bi-scg.png"/>
      <img class="_bi-inter" src="./imgs/bi-international.png"/>
      <p class="_intro">
        Expertise in complete<br class="show-md"/>
        <b>services and solutions</b><br/>
        with <b>international presences</b><br/>
        and <b>strong alliances.</b>
      </p>
      <div id="home-contact-btn" class="">
        <input id="home-email-input" type="email" placeholder="NAME@COMPANY.COM"/>
        <div class="btn-primary">GET IN TOUCH</div>
      </div>
      <img class="_people hide-md" src="./imgs/banner-people.png"/>
    </div>
    <img class="_people show-md" src="./imgs/banner-people.png"/>

    <div id="scroll-down">
      <div>scroll down</div>
      <div class="_v-line"></div>
    </div>
  </div>

  <!-- Section(sub) - Principle -->
  <div id="_sec-principle" class="layout-outer layout-padding-1">

    <div id="sec-principle-trigger"></div>

    <div class="layout-inner">
      <h1 class="header-section">OUR <span class="color1">PRINCIPLES</span></h1>
      <div class="principle-wrap">

        <div class="principle">
          <div class="_img __img-1"></div>
          <div class="_wrap">
            <h2 class="_title">Customer Centric</h2>
            <p class="_desc">
              With Customer Centric as the core of our work, by empathizing with our customers' stories and insights - we can provide the best services and solutions to suit their needs.
          </div>
        </div>
        <div class="principle">
          <div class="_img __img-2"></div>
          <div class="_wrap">
            <h2 class="_title">Data Driven</h2>
            <p class="_desc">
              Utilizing data is one of the most important philosophy of our people and  working processes which provides better services and solutions to satisfy customers.
          </div>
        </div>
        <div class="principle">
          <div class="_img __img-3"></div>
          <div class="_wrap">
            <h2 class="_title">Services and Solutions</h2>
            <p class="_desc">
              End-to-end services and solutions from our strong professional team and global partners have helped countless number of customers achieve business goal.
          </div>
        </div>

      </div>
    </div>
  </div>

  <!-- Section(sub) - Counter -->
  <div id="_sec-counter" class="">
    <!--    <div class="bg-under"></div>-->
    <div class="_outer layout-padding-1">
      <div class="counter-wrap">
        <!-- top -->
        <div class="top-wrap">
          <div class="_main-desc color1">
            We are participating to<br/>
            leverage the world economy
          </div>
          <div class="partner-count-wrap">
            <div class="_counter _counter-1">
              <div class="run-number-partner">0</div>
              <span class="_plus">+</span>
            </div>
            <div class="_caption">Business partners.</div>
          </div>
        </div>
        <!-- bottom -->
        <div class="bottom-wrap">
          <div class="_trade">
            <div class="_counter">
              <div class="run-number-trade">0</div>
              <span class="_plus">+</span></div>
            <div class="_caption">Trade containers.</div>
          </div>
          <div class="_turnover">
            <div class="_inner">
              <div class="_counter">
                <div class="_dollar-sign">$</div>
                <div class="run-number-turnover">0</div>
                <div class="_billions">billions+</div>
              </div>
              <div class="_caption">Turnover value.</div>
            </div>
          </div>
        </div>
      </div>
    </div>

  </div>

  <!--  <div id="mesh-principle"></div>-->
  <div id="particles-principle"></div>
</div>

<!-- Section - Solution -->
<div id="sec-solution" class="layout-full">
  <h1 class="header-section">
    <span class="color-white">SERVICES</span> <span class="color1">AND SOLUTIONS</span>
  </h1>
  <div class="solution-wrap">
    <!-- solution 1 -->
    <div class="solution">
      <div class="_img __ss-1">
        <video class="_video" autobuffer playsinline loop muted>
          <source src="./videos/ss1-720p-200.mp4" type="video/mp4">
          Your browser does not support the video tag.
        </video>
        <div class="_fg"></div>
      </div>
      <h2 class="_title"><span class="color1">commercial</span><br/>solution</h2>
      <ul>
        <li>Sourcing</li>
        <li>Construction</li>
        <li>Commodity</li>
        <li>Industrial</li>
      </ul>
    </div>
    <!-- solution 2 -->
    <div class="solution">
      <div class="_img __ss-2">
        <video class="_video" autobuffer playsinline loop muted>
          <source src="./videos/ss2-720p-200.mp4" type="video/mp4">
          Your browser does not support the video tag.
        </video>
        <div class="_fg"></div>
      </div>
      <h2 class="_title"><span class="color1">supply chain</span><br/>solution</h2>
      <ul>
        <li>Cross border logistic</li>
        <li>Business compliance</li>
        <li>Import – Export</li>
      </ul>
    </div>
    <!-- solution 3 -->
    <div class="solution">
      <div class="_img __ss-3">
        <video class="_video" autobuffer playsinline loop muted>
          <source src="./videos/ss3-720p-200.mp4" type="video/mp4">
          Your browser does not support the video tag.
        </video>
        <div class="_fg"></div>
      </div>
      <h2 class="_title"><span class="color1">technical</span><br/>solution</h2>
      <ul>
        <li>Smart Energy Solution</li>
        <li>Petrol Station Solution</li>
        <li>Industrial</li>
      </ul>
    </div>
    <!-- solution 4 -->
    <div class="solution">
      <div class="_img __ss-4">
        <video class="_video" autobuffer playsinline loop muted>
          <source src="./videos/ss4-720p-200.mp4" type="video/mp4">
          Your browser does not support the video tag.
        </video>
        <div class="_fg"></div>
      </div>
      <h2 class="_title"><span class="color1">financial</span><br/>solution</h2>
      <ul>
        <li>B2b Finance and Smart CRM</li>
      </ul>
    </div>
  </div>
</div>

<!-- Section - Presences -->
<div id="sec-presences" class="layout-full">
  <h1 class="header-section">INTERNATIONAL <span class="color1">PRESENCES</span></h1>
  <div class="presences-wrap">
    <img id="world-map" src="./imgs/world-map@2x.png"/>
    <img id="exploding-line" src="./imgs/world-line-dot.svg"/>
    <!--    <img id="exploding-line" src="./imgs/world-map@2x.png"/>-->
    <div class="stat-wrap">
      <div class="_num-global">21</div>
      <div class="_caption-global">Global Business Presences</div>
      <div class="bottom-wrap">
        <div class="stat-item">
          <div class="_num">12</div>
          <div class="_caption">companies</div>
        </div>
        <div class="stat-item">
          <div class="_num">4</div>
          <div class="_caption">branches</div>
        </div>
        <div class="stat-item">
          <div class="_num">5</div>
          <div class="_caption">representative offices*</div>
        </div>
      </div>
    </div>
  </div>
</div>

<!-- Section - Services -->
<div id="sec-products" class="layout-full">

  <!-- the mesh -->
  <!--  <div id="mesh-product"></div>-->
  <div id="particles-product"></div>
  <div id="bg-text-products">PRODUTCS<br/>SERVICES</div>

  <h1 class="header-section">
    WE CONTINUE TO DEVELOP <span class="color1">PRODUCTS AND SERVICES</span>
  </h1>
  <div id="product-carousel"></div>

  <div class="v-line __type-0"></div>

  <!-- Section - Partner -->
  <div id="_sec-partner" class="layout-full">
    <h1 class="header-section">PARTNERS AND <span class="color1">ALLIANCES</span></h1>
    <div class="_inner">
      <div class="partner-wrap">
        <div class="partner-logo">
          <img class="__kubota" src="./imgs/logo/logo-kubota.png"/>
        </div>
        <div class="partner-logo">
          <img class="__toyota" src="./imgs/logo/logo-toyota.png"/>
        </div>
        <div class="partner-logo">
          <img class="__cotto" src="./imgs/logo/logo-cotto.png"/>
        </div>
        <div class="partner-logo">
          <img class="__ralali" src="./imgs/logo/logo-ralali.png"/>
        </div>
        <div class="partner-logo">
          <img class="__mitsui" src="./imgs/logo/logo-mitsui.png"/>
        </div>
        <div class="partner-logo">
          <img class="__saison" src="./imgs/logo/logo-saison.png"/>
        </div>
        <div class="partner-logo">
          <img class="__sf" src="./imgs/logo/logo-sf-express.png"/>
        </div>
        <div class="partner-logo">
          <img class="__scg-logistic" src="./imgs/logo/logo-scg-logistic.png"/>
        </div>
        <div class="partner-logo">
          <img class="__great-wall" src="./imgs/logo/logo-great-wall.png"/>
        </div>
        <div class="partner-logo">
          <img class="__scg-express" src="./imgs/logo/logo-scg-express.png"/>
        </div>
        <div class="partner-logo">
          <img class="__bj" src="./imgs/logo/logo-bj.png"/>
        </div>
        <div class="partner-logo">
          <img class="__b2" src="./imgs/logo/logo-b2.png"/>
        </div>
        <div class="partner-logo">
          <img class="__siam-bangla" src="./imgs/logo/logo-siam-bangla.png"/>
        </div>
        <div class="partner-logo">
          <img class="__thaitheparos" src="./imgs/logo/logo-thaitheparos.png"/>
        </div>
        <div class="partner-logo">
          <img class="__tvi" src="./imgs/logo/logo-tvi.png"/>
        </div>
        <div class="partner-logo">
          <img class="__alcoa" src="./imgs/logo/logo-alcoa.png"/>
        </div>
        <div class="partner-logo">
          <img class="__betagro" src="./imgs/logo/logo-betagro.png"/>
        </div>
        <div class="partner-logo">
          <img class="__biller" src="./imgs/logo/logo-biller.png"/>
        </div>

        <!-- Next -->
        <div class="partner-logo">
          <img class="__canfor" src="./imgs/logo/logo-canfor.png"/>
        </div>
        <div class="partner-logo">
          <img class="__chememan" src="./imgs/logo/logo-chememan.png"/>
        </div>
        <div class="partner-logo">
          <img class="__chevron" src="./imgs/logo/logo-chevron.png"/>
        </div>
        <div class="partner-logo">
          <img class="__deestone" src="./imgs/logo/logo-deestone.png"/>
        </div>
        <div class="partner-logo">
          <img class="__domtar" src="./imgs/logo/logo-domtar.png"/>
        </div>
        <div class="partner-logo">
          <img class="__eat" src="./imgs/logo/logo-eat.png"/>
        </div>
        <div class="partner-logo">
          <img class="__knauf" src="./imgs/logo/logo-knauf.png"/>
        </div>
        <div class="partner-logo">
          <img class="__lixil" src="./imgs/logo/logo-lixil.png"/>
        </div>
        <div class="partner-logo">
          <img class="__bjc" src="./imgs/logo/logo-bjc.png"/>
        </div>
        <div class="partner-logo">
          <img class="__mondi" src="./imgs/logo/logo-mondi.png"/>
        </div>
        <div class="partner-logo">
          <img class="__nd-rubber" src="./imgs/logo/logo-nd-rubber.png"/>
        </div>
        <div class="partner-logo">
          <img class="__noritake" src="./imgs/logo/logo-noritake.png"/>
        </div>
        <div class="partner-logo">
          <img class="__novelis" src="./imgs/logo/logo-novelis.png"/>
        </div>
        <div class="partner-logo">
          <img class="__president-rice" src="./imgs/logo/logo-president-rice.png"/>
        </div>
        <div class="partner-logo">
          <img class="__saint-gobain" src="./imgs/logo/logo-saint-gobain.png"/>
        </div>
        <div class="partner-logo">
          <img class="__shell" src="./imgs/logo/logo-shell.png"/>
        </div>
        <div class="partner-logo">
          <img class="__suzano" src="./imgs/logo/logo-suzano.png"/>
        </div>
        <div class="partner-logo">
          <img class="__thaikk" src="./imgs/logo/logo-thaikk.png"/>
        </div>

      </div>
      <div class="v-line __type-1"></div>
    </div>
  </div>

  <!-- Section - Community -->
  <div id="_sec-community" class="layout-full">
    <div id="text-community">
      <h1 class="_com-header">
        <span class="color1">WITH OUR WIDE RANGE</span><br class="show-md"/>
        PRODUCTS & SERVICES,<br/>
        <span class="color1">NOT ONLY</span> BUSINESS<br class="show-md"/>
        <span class="color1">WILL STRIVE<br class="show-md"/> FOR THE BETTER.</span><br/>
        <span class="color1">THE</span> COMMUNITY <span class="color1">WILL TOO.</span>
      </h1>
      <div class="_because">
        Because we inspired to be a part of<br/>
        the community's growing economy and its livelihood.
      </div>
    </div>
  </div>

  <div id="bg-sec-community"></div>

</div>

<!-- Section - News -->
<div id="sec-news" class="layout-full">
  <div id="bg-text-news">ACTIVITIES</div>
  <h1 class="header-section">THE LATEST <span class="color1">NEWS</span></h1>
  <div id="news-carousel"></div>
</div>

<!-- Section - Contact -->
<div id="sec-contact">
  <div class="map-wrap">
    <iframe
        src="https://www.google.com/maps/embed?pb=!1m14!1m8!1m3!1d15498.797214108488!2d100.5262844!3d13.7969918!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x30e29c716e7047b7%3A0xe6b53da92980a5bd!2sSCG+Trading+Co.%2CLtd.!5e0!3m2!1sth!2sth!4v1547476127523"
        width="600" height="450" frameborder="0" style="border:0" allowfullscreen></iframe>
  </div>
  <div class="info-wrap">
    <img id="info-globe" src="./imgs/globe.png"/>
    <h2>
      <span class="color1">SCG</span> INTERNATIONAL<br class="hide-md"/>
      <span class="color1">Corporation<br class="show-xs"/> Co., Ltd.</span></h2>
    <h4>
      1 Siam Cement Road, Bangsue,<br class="hide-md"/>
      Bangkok 10800 Thailand
    </h4>
    <div class="contact-wrap">
      <div class="contact-item __map">
        <a href="https://goo.gl/maps/Hoy8YdKGsQF2" target="_blank">view in google map</a>
      </div>
      <div class="contact-item __tel">
        <a href="tel:+6625862222" target="_blank">+66-2586-2222</a>
      </div>
      <div class="contact-item __fax">
        <a href="tel:+6625862251" target="_blank">+66-2586-2251</a>
      </div>
      <div class="contact-item __mail">
        <a target="_blank">scginternational@scg.com</a>
      </div>
      <div id="contact-btn" class="btn-primary pop-contact">
        GET IN TOUCH
      </div>
    </div>
  </div>
</div>

<?php include "./footer.php" ?>

<!-- Scripts -->
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-easing/1.4.1/jquery.easing.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/babel-core/5.6.15/browser-polyfill.min.js"></script>

<script src="https://cdnjs.cloudflare.com/ajax/libs/gsap/2.0.2/TweenMax.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/ScrollMagic/2.0.6/ScrollMagic.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/ScrollMagic/2.0.6/plugins/debug.addIndicators.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/ScrollMagic/2.0.6/plugins/animation.gsap.min.js"></script>

    <script src="https://cdnjs.cloudflare.com/ajax/libs/slick-carousel/1.9.0/slick.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery.perfect-scrollbar/1.4.0/perfect-scrollbar.min.js"></script>
<script src="https://cdn.jsdelivr.net/npm/jquery-validation@1.17.0/dist/jquery.validate.min.js"></script>

<script src="./dist/app.bundle.js?<?php echo time(); ?>"></script>
<script src="./assets/particles.js"></script>

<?php include "./popups/popup-policy.php" ?>
<?php include "./popups/popup-news.php" ?>
<?php include "./popups/popup-contact.php" ?>

</body>
</html>


